import numpy as np
from Project.ProjectFramework import ProjectFramework


class Results(ProjectFramework):
    def __init__(self, name="Test1"):
        # Time
        self.time = np.array([])
        self.lap_time = np.array([])

        # Distance
        self.distance = np.array([])

        # Velocity
        self.longitudinal_velocity = np.array([])
        self.lateral_velocity = np.array([])

        # Acceleration
        self.longitudinal_acceleration = np.array([])
        self.lateral_acceleration = np.array([])

        # Aerodynamics
        self.aerodynamic_drag = np.array([])
        self.aerodynamic_downforce = np.array([])

        ProjectFramework.__init__(self, name, ".rest", "0.0.0")

    def import_result(self, Vel, s, t, lap_time):

        self.longitudinal_velocity = Vel
        self.distance = s
        self.time = t
        self.lap_time = lap_time
