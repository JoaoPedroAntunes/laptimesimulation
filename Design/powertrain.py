from Project.ProjectFramework import ProjectFramework

class Powertrain(ProjectFramework):
    def __init__(self, name="Test1"):
        self.power = 600                        # [kW]
        ProjectFramework.__init__(self, name, ".pwr", "0.0.0")

    def load_object(self, file):
        data = self.load_from_json(file)

        # Get the parameters from the json file.
        self.power = data.get("power")

    def save_object(self, file_path=""):
        data = {
            "name": self.name,
            "version": self.version,
            "date": self.current_date_time(),
            "user": self.get_user(),
            "power": self.power,
        }

        self.save_to_json(file_path, data)  # Call the json method to save the file