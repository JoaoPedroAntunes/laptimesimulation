from Project.ProjectFramework import ProjectFramework

class Aerodynamics(ProjectFramework):
    def __init__(self, name="Test1"):
        self.lift_coefficient = 5               # [-]
        self.drag_coefficient = 1.3             # [-]
        self.frontal_area = 1                   # [m^2]
        self.air_density = 1.22          # air density, [kg/m^3]

        ProjectFramework.__init__(self, name, ".aer", "0.0.0")

    def load_object(self, file):
        data = self.load_from_json(file)

        # Get the parameters from the json file.
        self.air_density = data.get("air_density")
        self.lift_coefficient = data.get("lift_coefficient")
        self.frontal_area = data.get("frontal_area")
        self.drag_coefficient = data.get("drag_coefficient")

    def save_object(self, file_path=""):
        data = {
            "name": self.name,
            "version": self.version,
            "date": self.current_date_time(),
            "user": self.get_user(),
            "air_density": self.air_density,
            "frontal_area": self.frontal_area,
            "lift_coefficient": self.lift_coefficient,
            "drag_coefficient": self.drag_coefficient
        }

        self.save_to_json(file_path, data)  # Call the json method to save the file

    def get_CL_A(self):
        return self.lift_coefficient * self.frontal_area

    def get_Cd_A(self):
        return self.drag_coefficient * self.frontal_area
