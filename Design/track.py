import json
import logging

import numpy as np
import matplotlib.pyplot as plt
from Import_Export import compute_curvature
from Project.ProjectFramework import ProjectFramework
import csv

class Track(ProjectFramework):
    def __init__(self, name="Test1"):
        self.circuit = np.array([])
        self.curvature = np.array([])
        ProjectFramework.__init__(self, name, ".cir", "0.0.0")

    def import_track(self, CircuitName):
        """
        Imports the track coordinates.
        :param: CircuitName:    Name of the file containing points of the track, in the main folder
        """
        self.circuit = np.loadtxt(CircuitName)
        self.name = CircuitName

    def compute_curvature(self):
        """
        Computes curvature point per point.
        """
        self.curvature, _, _, _ = compute_curvature(self.circuit)

    def plot_track(self):
        """
        Plots the track.
        """
        fig, ax = plt.subplots()
        ax.plot(self.circuit[:, 0], self.circuit[:, 1])
        ax.legend([self.name])
        plt.xlabel("Longitudinal Coordinate [m]")
        plt.ylabel("Lateral Coordinate [m]")
        plt.show()

    def plot_curvature(self):
        """
        Plots the track with the curvature
        """
        fig, ax = plt.subplots()
        ax.scatter(self.circuit[:, 0], self.circuit[:, 1], c=self.curvature[:, 0])
        ax.legend([self.name])
        plt.xlabel("Longitudinal Coordinate [m]")
        plt.ylabel("Lateral Coordinate [m]")
        plt.show()

    def load_object(self, file):
        # I've purposefully modified the object so that we can add a header.
        try:

            with open(file, "r") as read_file:
                for line in read_file:
                    data = json.loads(line)
                    self.name = data.get("name")
                    self.version = data.get("version")
                    self.date = data.get("date")
                    self.user = data.get("user")
                    break

        except Exception as ex:
            logging.exception('The following file was not successfully loaded: ' + self.name + self.extension)

        with open(file, newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
            next(spamreader)
            firstrow = []
            secondrow=[]

            for row in spamreader:
                firstrow.append(float(row[0]))
                secondrow.append(float(row[1]))

            self.circuit = np.column_stack((firstrow,secondrow))






    def save_object(self, file_path=""):
        data = {
            "name": self.name,
            "version": self.version,
            "date": self.current_date_time(),
            "user": self.get_user(),
        }

        self.save_to_json(file_path, data)  # Call the json method to save the file
        with open(self.name + self.extension, 'a', newline='') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            spamwriter.writerow("")
            for i in range(len(self.circuit[:,0])):
                spamwriter.writerow([self.circuit[i,0], self.circuit[i,1]])



class Logged_Data:
    def __init__(self):

        self.logged = np.array([])

    def import_logged(self, LoggedName):
        """
        Imports logged data. Appends new logged data to the logged data array.
        :param: LoggedName:     Name of the file containing logged data, in the main folder
        """
        log = np.loadtxt(LoggedName).reshape(-1, 1)
        if self.logged.size == 0:
            self.logged = log
        else:
            self.logged = np.append(self.logged, log, axis=1)

