from Design import Chassis, Aerodynamics, Tyres, Powertrain, Results
from Project.ProjectFramework import ProjectFramework
from Project.ProjectFramework import DesignDirectory


class Vehicle(ProjectFramework):
    def __init__(self,
                 chassis: Chassis,
                 aerodynamics: Aerodynamics,
                 tyres: Tyres,
                 powertrain: Powertrain,
                 name = "Test1"):

        self.chassis = chassis
        self.aerodynamics = aerodynamics
        self.tyres = tyres
        self.powertrain = powertrain
        self.results = Results()

        ProjectFramework.__init__(self, name, ".veh", "0.0.0")

    def load_object(self, file_path):
        data = self.load_from_json(file_path)

        self.chassis.name = data.get("basic_properties_name")
        self.aerodynamics.name = data.get("aerodynamics_name")
        self.tyres.name = data.get("suspension_front_name")
        self.powertrain.name = data.get("suspension_rear_name")

        # Remove the last item so that we can get the project directory
        file_path_split = str(file_path).split("\\")
        project_directory = ""
        for i in range(0, len(file_path_split) - 2):
            if i == 0:
                project_directory = file_path_split[i]
            else:
                project_directory = project_directory + "\\" + file_path_split[i]

        # TODO test if this is working
        if not self.chassis.name == "":
            self.chassis.load_object(
                project_directory + DesignDirectory.basic_properties.value + "\\" + self.chassis.name + self.chassis.extension)

        if not self.aerodynamics.name == "":
            self.aerodynamics.load_object(
                project_directory + DesignDirectory.aerodynamics.value + "\\" + self.aerodynamics.name + self.aerodynamics.extension)
        # Suspension
        if not self.tyres.name == "":
            self.tyres.load_object(
                project_directory + DesignDirectory.suspension.value + "\\" + self.tyres.name + self.tyres.extension)

        if not self.powertrain.name == "":
            self.powertrain.load_object(
                project_directory + DesignDirectory.suspension.value + "\\" + self.powertrain.name + self.powertrain.extension)


    def save_object(self, file_path=""):
        data = {
            "name": self.name,
            "version": self.version,
            "date": self.current_date_time(),
            "user": self.get_user(),
            "chassis_name" : self.chassis.name,
            "aerodynamics_name": self.aerodynamics.name,
            "tyres_name": self.tyres.name,
            "powertrain_name": self.powertrain.name,
            }

        self.save_to_json(file_path, data)
