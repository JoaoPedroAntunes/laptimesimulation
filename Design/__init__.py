from Design.tyres import Tyres
from Design.aerodynamics import Aerodynamics
from Design.chassis import Chassis
from Design.powertrain import Powertrain
from Design.results import Results
from Design.vehicle import Vehicle
