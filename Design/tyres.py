from Project.ProjectFramework import ProjectFramework

class Tyres(ProjectFramework):
    def __init__(self, name="Test1"):
        self.longitudinal_friction = 1.4        # [-]
        self.lateral_friction = 1.4             # [-]
        ProjectFramework.__init__(self, name, ".tyr", "0.0.0")

    def load_object(self, file):
        data = self.load_from_json(file)

        # Get the parameters from the json file.
        self.longitudinal_friction = data.get("longitudinal_friction")
        self.lateral_friction = data.get("lateral_friction")


    def save_object(self, file_path=""):
        data = {
            "name": self.name,
            "version": self.version,
            "date": self.current_date_time(),
            "user": self.get_user(),
            "longitudinal_friction": self.longitudinal_friction,
            "lateral_friction": self.lateral_friction,
        }

        self.save_to_json(file_path, data)  # Call the json method to save the file
