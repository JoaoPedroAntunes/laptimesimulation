import os
from pathlib import Path

from Design.track import Logged_Data
from Design.track import Track
from Design.vehicle import Vehicle
from Design.chassis import Chassis
from Design.aerodynamics import Aerodynamics
from Design.tyres import Tyres
from Design.powertrain import Powertrain
from Project.ProjectFramework import ProjectFramework
from Solver.lap_time_sim import lap_time_sim
from UserInterface.TreeWidget.TreeWidgetManager import TreeViewNames
from UserInterface.main_window_W import main_window_W

CircuitName = 'Silverstone.dat'
LoggedName = 'Silverstone_speed.dat'
car = Vehicle(Chassis(), Aerodynamics(), Tyres(), Powertrain())

silverstone = Track()
silverstone.import_track(CircuitName)

logged = Logged_Data()
logged.import_logged(LoggedName)


#Vel, s, t, lap_time = lap_time_sim(car, silverstone)

#minutes = int(lap_time // 60)
#seconds = round(float(lap_time % 60), 3)

#print(minutes, seconds, sep=':')


#To not open the user interface just comment this code below
if __name__ == "__main__":
    main_window_W = main_window_W()
    project_name = "Test"
    file_path = str(Path.home()) + "/Documents/test55"

    # If folder doesn't exist create the folder and populate it with a demo project
    if not os.path.exists(file_path):
        foo = ProjectFramework("test", ".wt", "0.0.0")
        foo.create_project_folders(project_name, file_path)

        # Create 3 files for each folder
        name_list = ["Test1", "Test2", "Test3"]
        for name in name_list:
            aero = Aerodynamics()
            aero.name = name
            aero.save_object(file_path + "\\" + TreeViewNames.aerodynamics.value)

            bp = Chassis()
            bp.name = name
            bp.save_object(file_path + "\\" + TreeViewNames.chassis.value)

            tire = Tyres()
            tire.name = name
            tire.save_object(file_path + "\\" + TreeViewNames.tyre.value)

            powertrain = Powertrain()
            powertrain.name = name
            powertrain.save_object(file_path + "\\" + TreeViewNames.powertrain.value)



    else:
        main_window_W.open_project(file_path + "/" + "Test55.wt")

    main_window_W.show()
