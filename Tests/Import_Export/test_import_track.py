from Import_Export import Track, Logged_Data
from Import_Export.computecurv import compute_curvature
import matplotlib.pyplot as plt


def test_import_track():
    CircuitName = 'Silverstone'
    LoggedName = 'Silverstone_speed'

    silverstone = Track()
    silverstone.import_track(CircuitName)

    logged = Logged_Data()
    logged.import_logged(LoggedName)

    silverstone.compute_curvature()

    print("")
    print("Circuit")
    print(silverstone)
    print("")

    print("Logged Data")
    print(logged)
    print("")

    print("Curvature")

    silverstone.plot_track()
    # fig, ax = plt.subplots()
    # ax.plot(silverstone.circuit[:, 0], silverstone.circuit[:, 1])
    # plt.show()
