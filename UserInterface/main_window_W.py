from Project.ProjectFramework import ProjectFramework
import os
import sys
from pathlib import Path
from PyQt5 import QtWidgets, QtCore

from UserInterface.QT_UI.main_window_ui import Ui_MainWindow
from UserInterface.TreeWidget.TreeWidgetManager import TreeWidgetManager, TreeViewNames


class main_window_W(ProjectFramework):
    def __init__(self):
        self.app = QtWidgets.QApplication(sys.argv)
        self.MainWindow = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self.MainWindow)

        # Create the project Tree Widget
        self.ui.treeWidget_project = TreeWidgetManager(self.ui.tab)
        self.ui.treeWidget_project.setObjectName("treeWidget_project")
        self.ui.treeWidget_project.headerItem().setText(0, "1")
        self.ui.treeWidget_project.header().setVisible(False)
        self.ui.verticalLayout.addWidget(self.ui.treeWidget_project)

        self.ui.treeWidget_project.itemDoubleClicked.connect(self.ui.treeWidget_project.on_itemDoubleClicked)

        ProjectFramework.__init__(self, "Test", ".wt", "0.0.0")

    def __set_project_directory__(self, path):
        self.project_directory = path
        self.ui.treeWidget_project.project_directory = path

    def open_project(self, file_path_directory=""):

        # If not directory is specified in the input then create a pop-up window to ask the user for a project to open
        if file_path_directory == False:  # The False statement is necessary because when it is called by QT it returns a bolean and not a string
            options = QtWidgets.QFileDialog.Options()
            options |= QtWidgets.QFileDialog.DontUseNativeDialog
            # TODO change the directory to be a default one from the user.
            desktop_path = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop')
            file_path_directory = QtWidgets.QFileDialog.getOpenFileName(None, "Create Project",
                                                                        desktop_path,
                                                                        "WT (*.wt)",
                                                                        options=options)
            # Remove the second item that we don't need
            file_path_directory = file_path_directory[0]

            # If the file_path_directory is empty it means that the user canceled thus return. If it is false it will run the remaining code
            if file_path_directory == "": return

        # When the user opens a project we need to remove the previous one
        self.ui.treeWidget_project.clear()

        # Remove the last item so that we can get the project directory
        file_path_split = str(file_path_directory).split("/")
        self.__set_project_directory__("")
        for i in range(0, len(file_path_split) - 1):
            if i == 0:
                self.__set_project_directory__(file_path_split[i])
            else:
                self.__set_project_directory__(self.project_directory + "\\" + file_path_split[i])

        self.ui.treeWidget_project.create_master_tree_nodes(TreeViewNames)


        # Load the object from the project into the tree nodes
        self.ui.treeWidget_project.load_child_nodes(self.project_directory, TreeViewNames)

        self.ui.treeWidget_project.expandAll()

    def show(self):
        self.MainWindow.show()
        sys.exit(self.app.exec_())