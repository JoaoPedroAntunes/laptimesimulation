# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'vehicle_ui.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Vehicle(object):
    def setupUi(self, Vehicle):
        Vehicle.setObjectName("Vehicle")
        Vehicle.resize(640, 480)
        self.verticalLayout = QtWidgets.QVBoxLayout(Vehicle)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox = QtWidgets.QGroupBox(Vehicle)
        self.groupBox.setObjectName("groupBox")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.groupBox)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.frame_4 = QtWidgets.QFrame(self.groupBox)
        self.frame_4.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_4.setObjectName("frame_4")
        self.gridLayout = QtWidgets.QGridLayout(self.frame_4)
        self.gridLayout.setObjectName("gridLayout")
        self.comboBox_basic_properties = QtWidgets.QComboBox(self.frame_4)
        self.comboBox_basic_properties.setMinimumSize(QtCore.QSize(100, 0))
        self.comboBox_basic_properties.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.comboBox_basic_properties.setObjectName("comboBox_basic_properties")
        self.gridLayout.addWidget(self.comboBox_basic_properties, 2, 1, 1, 1)
        self.label_chassis = QtWidgets.QLabel(self.frame_4)
        self.label_chassis.setObjectName("label_chassis")
        self.gridLayout.addWidget(self.label_chassis, 2, 0, 1, 1)
        self.label_aerodynamics = QtWidgets.QLabel(self.frame_4)
        self.label_aerodynamics.setObjectName("label_aerodynamics")
        self.gridLayout.addWidget(self.label_aerodynamics, 5, 0, 1, 1)
        self.comboBox_aerodynamics = QtWidgets.QComboBox(self.frame_4)
        self.comboBox_aerodynamics.setObjectName("comboBox_aerodynamics")
        self.gridLayout.addWidget(self.comboBox_aerodynamics, 5, 1, 1, 1)
        self.comboBox_tire = QtWidgets.QComboBox(self.frame_4)
        self.comboBox_tire.setObjectName("comboBox_tire")
        self.gridLayout.addWidget(self.comboBox_tire, 6, 1, 1, 1)
        self.label = QtWidgets.QLabel(self.frame_4)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 6, 0, 1, 1)
        self.horizontalLayout_2.addWidget(self.frame_4, 0, QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.frame_5 = QtWidgets.QFrame(self.groupBox)
        self.frame_5.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_5.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_5.setObjectName("frame_5")
        self.line = QtWidgets.QFrame(self.frame_5)
        self.line.setGeometry(QtCore.QRect(10, 162, 394, 16))
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.horizontalLayout_2.addWidget(self.frame_5)
        self.verticalLayout.addWidget(self.groupBox)
        self.groupBox_2 = QtWidgets.QGroupBox(Vehicle)
        self.groupBox_2.setMaximumSize(QtCore.QSize(16777215, 50))
        self.groupBox_2.setObjectName("groupBox_2")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.groupBox_2)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.Label_Obj_Name = QtWidgets.QLabel(self.groupBox_2)
        self.Label_Obj_Name.setObjectName("Label_Obj_Name")
        self.horizontalLayout.addWidget(self.Label_Obj_Name, 0, QtCore.Qt.AlignRight)
        self.lineEdit_Name = QtWidgets.QLineEdit(self.groupBox_2)
        self.lineEdit_Name.setObjectName("lineEdit_Name")
        self.horizontalLayout.addWidget(self.lineEdit_Name, 0, QtCore.Qt.AlignRight)
        self.pushButton_OK = QtWidgets.QPushButton(self.groupBox_2)
        self.pushButton_OK.setObjectName("pushButton_OK")
        self.horizontalLayout.addWidget(self.pushButton_OK, 0, QtCore.Qt.AlignRight)
        self.verticalLayout.addWidget(self.groupBox_2)

        self.retranslateUi(Vehicle)
        QtCore.QMetaObject.connectSlotsByName(Vehicle)

    def retranslateUi(self, Vehicle):
        _translate = QtCore.QCoreApplication.translate
        Vehicle.setWindowTitle(_translate("Vehicle", "Form"))
        self.label_chassis.setText(_translate("Vehicle", "Chassis"))
        self.label_aerodynamics.setText(_translate("Vehicle", "Aerodynamics"))
        self.label.setText(_translate("Vehicle", "Tire"))
        self.Label_Obj_Name.setText(_translate("Vehicle", "Name:"))
        self.pushButton_OK.setText(_translate("Vehicle", "OK"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Vehicle = QtWidgets.QWidget()
    ui = Ui_Vehicle()
    ui.setupUi(Vehicle)
    Vehicle.show()
    sys.exit(app.exec_())
