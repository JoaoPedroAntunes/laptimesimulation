# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'tire_ui.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Tire(object):
    def setupUi(self, Tire):
        Tire.setObjectName("Tire")
        Tire.resize(640, 480)
        self.verticalLayout = QtWidgets.QVBoxLayout(Tire)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox = QtWidgets.QGroupBox(Tire)
        self.groupBox.setObjectName("groupBox")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.groupBox)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.frame_4 = QtWidgets.QFrame(self.groupBox)
        self.frame_4.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_4.setObjectName("frame_4")
        self.gridLayout = QtWidgets.QGridLayout(self.frame_4)
        self.gridLayout.setObjectName("gridLayout")
        self.label_12 = QtWidgets.QLabel(self.frame_4)
        self.label_12.setObjectName("label_12")
        self.gridLayout.addWidget(self.label_12, 0, 2, 1, 1)
        self.lineEdit_Stiffness = QtWidgets.QLineEdit(self.frame_4)
        self.lineEdit_Stiffness.setMaximumSize(QtCore.QSize(75, 16777215))
        self.lineEdit_Stiffness.setAlignment(QtCore.Qt.AlignCenter)
        self.lineEdit_Stiffness.setObjectName("lineEdit_Stiffness")
        self.gridLayout.addWidget(self.lineEdit_Stiffness, 0, 1, 1, 1, QtCore.Qt.AlignTop)
        self.label_long_coef = QtWidgets.QLabel(self.frame_4)
        self.label_long_coef.setObjectName("label_long_coef")
        self.gridLayout.addWidget(self.label_long_coef, 0, 0, 1, 1)
        self.lineEdit = QtWidgets.QLineEdit(self.frame_4)
        self.lineEdit.setMaximumSize(QtCore.QSize(75, 16777215))
        self.lineEdit.setObjectName("lineEdit")
        self.gridLayout.addWidget(self.lineEdit, 1, 1, 1, 1)
        self.label_lat_coef = QtWidgets.QLabel(self.frame_4)
        self.label_lat_coef.setObjectName("label_lat_coef")
        self.gridLayout.addWidget(self.label_lat_coef, 1, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.frame_4)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 2, 1, 1)
        self.horizontalLayout_2.addWidget(self.frame_4, 0, QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.frame_5 = QtWidgets.QFrame(self.groupBox)
        self.frame_5.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_5.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_5.setObjectName("frame_5")
        self.line = QtWidgets.QFrame(self.frame_5)
        self.line.setGeometry(QtCore.QRect(10, 162, 394, 16))
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.horizontalLayout_2.addWidget(self.frame_5)
        self.verticalLayout.addWidget(self.groupBox)
        self.groupBox_2 = QtWidgets.QGroupBox(Tire)
        self.groupBox_2.setMaximumSize(QtCore.QSize(16777215, 50))
        self.groupBox_2.setObjectName("groupBox_2")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.groupBox_2)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.Label_Obj_Name = QtWidgets.QLabel(self.groupBox_2)
        self.Label_Obj_Name.setObjectName("Label_Obj_Name")
        self.horizontalLayout.addWidget(self.Label_Obj_Name, 0, QtCore.Qt.AlignRight)
        self.lineEdit_Name = QtWidgets.QLineEdit(self.groupBox_2)
        self.lineEdit_Name.setObjectName("lineEdit_Name")
        self.horizontalLayout.addWidget(self.lineEdit_Name, 0, QtCore.Qt.AlignRight)
        self.pushButton_OK = QtWidgets.QPushButton(self.groupBox_2)
        self.pushButton_OK.setObjectName("pushButton_OK")
        self.horizontalLayout.addWidget(self.pushButton_OK, 0, QtCore.Qt.AlignRight)
        self.verticalLayout.addWidget(self.groupBox_2)

        self.retranslateUi(Tire)
        QtCore.QMetaObject.connectSlotsByName(Tire)

    def retranslateUi(self, Tire):
        _translate = QtCore.QCoreApplication.translate
        Tire.setWindowTitle(_translate("Tire", "Form"))
        self.label_12.setText(_translate("Tire", "TextLabel"))
        self.label_long_coef.setText(_translate("Tire", "Longitudinal coefficient"))
        self.label_lat_coef.setText(_translate("Tire", "Lateral coefficient"))
        self.label_2.setText(_translate("Tire", "TextLabel"))
        self.Label_Obj_Name.setText(_translate("Tire", "Name:"))
        self.pushButton_OK.setText(_translate("Tire", "OK"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Tire = QtWidgets.QWidget()
    ui = Ui_Tire()
    ui.setupUi(Tire)
    Tire.show()
    sys.exit(app.exec_())
