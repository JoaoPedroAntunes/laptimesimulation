# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'circuit_ui.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_track(object):
    def setupUi(self, Aerodynamics):
        Aerodynamics.setObjectName("Aerodynamics")
        Aerodynamics.resize(640, 480)
        self.verticalLayout = QtWidgets.QVBoxLayout(Aerodynamics)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox = QtWidgets.QGroupBox(Aerodynamics)
        self.groupBox.setObjectName("groupBox")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.groupBox)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.pushButton = QtWidgets.QPushButton(self.groupBox)
        self.pushButton.setMaximumSize(QtCore.QSize(350, 16777215))
        self.pushButton.setObjectName("pushButton")
        self.verticalLayout_3.addWidget(self.pushButton)
        self.tableView = QtWidgets.QTableView(self.groupBox)
        self.tableView.setMaximumSize(QtCore.QSize(350, 16777215))
        self.tableView.setObjectName("tableView")
        self.verticalLayout_3.addWidget(self.tableView)
        self.horizontalLayout_2.addLayout(self.verticalLayout_3)
        self.verticalLayout.addWidget(self.groupBox)
        self.groupBox_2 = QtWidgets.QGroupBox(Aerodynamics)
        self.groupBox_2.setMaximumSize(QtCore.QSize(16777215, 50))
        self.groupBox_2.setObjectName("groupBox_2")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.groupBox_2)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.Label_Obj_Name = QtWidgets.QLabel(self.groupBox_2)
        self.Label_Obj_Name.setObjectName("Label_Obj_Name")
        self.horizontalLayout.addWidget(self.Label_Obj_Name, 0, QtCore.Qt.AlignRight)
        self.lineEdit_Name = QtWidgets.QLineEdit(self.groupBox_2)
        self.lineEdit_Name.setObjectName("lineEdit_Name")
        self.horizontalLayout.addWidget(self.lineEdit_Name, 0, QtCore.Qt.AlignRight)
        self.pushButton_OK = QtWidgets.QPushButton(self.groupBox_2)
        self.pushButton_OK.setObjectName("pushButton_OK")
        self.horizontalLayout.addWidget(self.pushButton_OK, 0, QtCore.Qt.AlignRight)
        self.verticalLayout.addWidget(self.groupBox_2)

        self.retranslateUi(Aerodynamics)
        QtCore.QMetaObject.connectSlotsByName(Aerodynamics)

    def retranslateUi(self, Aerodynamics):
        _translate = QtCore.QCoreApplication.translate
        Aerodynamics.setWindowTitle(_translate("Aerodynamics", "Form"))
        self.pushButton.setText(_translate("Aerodynamics", "Import Track"))
        self.Label_Obj_Name.setText(_translate("Aerodynamics", "Name:"))
        self.pushButton_OK.setText(_translate("Aerodynamics", "OK"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Aerodynamics = QtWidgets.QWidget()
    ui = Ui_track()
    ui.setupUi(Aerodynamics)
    Aerodynamics.show()
    sys.exit(app.exec_())
