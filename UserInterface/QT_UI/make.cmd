:: Remove it printing out every step
@echo off & setlocal

pyuic5 -x aerodynamics_ui.ui -o aerodynamics_ui.py
pyuic5 -x chassis_ui.ui -o chassis_ui.py
pyuic5 -x tire_ui.ui -o tire_ui.py
pyuic5 -x vehicle_ui.ui -o vehicle_ui.py
pyuic5 -x mainwindow.ui -o main_window_ui.py
pyuic5 -x powertrain_ui.ui -o powertrain_ui.py
pyuic5 -x circuit_ui.ui -o circuit_ui.py

Echo Done