import os
from enum import Enum

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QTreeWidget, QMessageBox, QTreeWidgetItem

from Design.powertrain import Powertrain
from Design.aerodynamics import Aerodynamics
from Design.chassis import Chassis
from Design.track import Track
from Design.tyres import Tyres
from Design.vehicle import Vehicle
from Project.ProjectFramework import ProjectFramework
from UserInterface.Design import aerodynamics_W
from UserInterface.Design import aerodynamics_W
from UserInterface.Design import chassis_W
from UserInterface.Design import tire_W
from UserInterface.Design import vehicle_W
from UserInterface.Design import powertrain_W
from UserInterface.Design.track_W import track_W


class TreeViewNames(Enum):
    circuit = "Circuit"
    aerodynamics = "Aerodynamics"
    chassis = "Chassis"
    tyre = "Tyres"
    powertrain = "Powertrain"
    vehicle = "Vehicle"


class TreeWidgetManager(QTreeWidget):
    def __init__(self, tab):
        super().__init__(tab)
        self.project_directory = ""


    def is_name_already_exists(self, parent_node_name, name) -> bool:
        # Get parent index based on the name
        index = self.__get_parent_index_from_name__(parent_node_name)

        # Loop inside the child nodes to see if the name alreayd exists
        for i in range(self.topLevelItem(index).childCount()):
            if self.topLevelItem(index).child(i).text(0) == name:
                return True

        return False

    def msg_box_name_already_exists(self):
        msg_box = QMessageBox()
        msg_box.setIcon(QMessageBox.Question)
        msg_box.setText("The name already exists")
        msg_box.setInformativeText("Do you want to override it?")
        msg_box.setStandardButtons(QMessageBox.Save | QMessageBox.Discard | QMessageBox.Cancel)
        msg_box.setDefaultButton(QMessageBox.Save)
        ret = msg_box.exec_()
        return ret

    def add_obj_to_tree(self, obj_name, parent_node_name):
        index = self.__get_parent_index_from_name__(parent_node_name)

        # Add this new node to the treeview
        sub_level_item = QTreeWidgetItem(self.topLevelItem(index))
        sub_level_item.setText(0, obj_name)

    def __get_parent_index_from_name__(self, parent_node_name) -> int:
        for i in range(self.topLevelItemCount()):

            if self.topLevelItem(i).text(0) == parent_node_name:
                return i

        raise Exception("Couldn't find the parent node")

    def delete_node(self, node):
        node.parent().removeChild(node)

    def __get_childs_name__(self, parent_node_name) -> list:
        index = self.__get_parent_index_from_name__(parent_node_name)
        # Loop inside the child nodes to see if the name alreayd exists
        out_list = []
        for i in range(self.topLevelItem(index).childCount()):
            out_list.append(str(self.topLevelItem(index).child(i).text(0)))

        return out_list

    def get_checked_childs(self, parent_node_name):
        index = self.__get_parent_index_from_name__(parent_node_name)
        out_list = []
        for i in range(self.topLevelItem(index).childCount()):
            if self.topLevelItem(index).child(i).checkState(0) == QtCore.Qt.Checked:
                out_list.append(self.topLevelItem(index).child(i).text(0))

        return out_list

    def create_master_tree_nodes(self, parent_node_names: TreeViewNames):
        for i, results_dic in enumerate(parent_node_names):
            top_level_item = QtWidgets.QTreeWidgetItem(self)
            top_level_item.setText(0, results_dic.value)

    def load_child_nodes(self, project_directory, parent_node_names: TreeViewNames):
        for i, results_dic in enumerate(parent_node_names):
            obj_in_folder = os.listdir(project_directory + "\\" + results_dic.value)

            for name in obj_in_folder:
                sub_level_item = QtWidgets.QTreeWidgetItem(
                    self.topLevelItem(self.__get_parent_index_from_name__(results_dic.value)))
                obj_loaded = ProjectFramework("", "", "")
                obj_loaded.load_from_json(project_directory + "\\" + results_dic.value + "\\" + name)
                sub_level_item.setText(0, obj_loaded.name)

    def on_itemDoubleClicked(self, item):
        if (item.parent()):
            self.open_object(item)

    def menuContextTree(self, point):
        # Infos about the node selected.
        index = self.indexAt(point)

        if not index.isValid():
            return

        item = self.itemAt(point)
        name = item.text(0)  # The text of the node.

        if (not item.parent()):

            # We build the menu.
            menu = QtWidgets.QMenu()
            action_create = menu.addAction("Create")
            menu.addSeparator()
            action_import = menu.addAction("Import")

            action = menu.exec_(self.mapToGlobal(point))

            # Todo finish the correct actions to perform
            if action == action_create:
                self.create_object(item)
            elif action == action_import:
                pass

        else:

            # Todo finish the submenu for the items
            # We build the menu.
            menu = QtWidgets.QMenu()
            action_open = menu.addAction("Open")
            menu.addSeparator()
            action_duplicate = menu.addAction("Duplicate")
            action_rename = menu.addAction("Rename")
            menu.addSeparator()
            action_delete = menu.addAction("Delete")

            action = menu.exec_(self.mapToGlobal(point))

            if action == action_open:
                self.open_object(item)
            elif action == action_delete:
                self.delete_object(item)

    def create_object(self, node):
        if node.text(0) == TreeViewNames.aerodynamics.value:
            self.obj_form = aerodynamics_W(None, self.project_directory, self)
            self.obj_form.Form.show()

        elif node.text(0) == TreeViewNames.basic_properties.value:
            self.obj_form = chassis_W(None, self.project_directory, self)
            self.obj_form.Form.show()

        elif node.text(0) == TreeViewNames.inputs.value:
            self.obj_form = powertrain_W(None, self.project_directory, self)
            self.obj_form.Form.show()

        elif node.text(0) == TreeViewNames.tire.value:
            self.obj_form = tire_W(None, self.project_directory, self)
            self.obj_form.Form.show()

        elif node.text(0) == TreeViewNames.vehicle.value:
            self.obj_form = vehicle_W(None, self.project_directory, self)
            self.obj_form.Form.show()



    # Todo Implemte the rest
    def open_object(self, node):
        if node.parent().text(0) == TreeViewNames.aerodynamics.value:
            obj_loaded = Aerodynamics()
            obj_loaded.load_object(
                self.project_directory + "\\" + TreeViewNames.aerodynamics.value + "\\" + node.text(
                    0) + obj_loaded.extension)
            self.obj_form = aerodynamics_W(obj_loaded, self.project_directory, self)
            self.obj_form.Form.show()

        elif node.parent().text(0) == TreeViewNames.chassis.value:
            obj_loaded = Chassis()
            obj_loaded.load_object(
                self.project_directory + "\\" + TreeViewNames.basic_properties.value + "\\" + node.text(
                    0) + obj_loaded.extension)
            self.obj_form =  chassis_W(obj_loaded, self.project_directory, self)
            self.obj_form.Form.show()

        elif node.parent().text(0) == TreeViewNames.powertrain.value:
            obj_loaded = Powertrain()
            obj_loaded.load_object(
                self.project_directory + "\\" + TreeViewNames.powertrain.value + "\\" + node.text(
                    0) + obj_loaded.extension)
            self.obj_form = powertrain_W(obj_loaded, self.project_directory, self)
            self.obj_form.Form.show()

        elif node.parent().text(0) == TreeViewNames.circuit.value:
            obj_loaded = Track()
            obj_loaded.load_object(
                self.project_directory + "\\" + TreeViewNames.circuit.value + "\\" + node.text(
                    0) + obj_loaded.extension)
            self.obj_form = track_W(obj_loaded, self.project_directory, self)
            self.obj_form.Form.show()


        elif node.parent().text(0) == TreeViewNames.tyre.value:
            obj_loaded = Tyres()
            obj_loaded.load_object(
                self.project_directory + "\\" + TreeViewNames.tire.value + "\\" + node.text(
                    0) + obj_loaded.extension)
            self.obj_form = tire_W(obj_loaded, self.project_directory, self)
            self.obj_form.Form.show()


        elif node.parent().text(0) == TreeViewNames.vehicle.value:
            obj_loaded = Vehicle()
            obj_loaded.load_object(
                self.project_directory + "\\" + TreeViewNames.vehicle.value + "\\" + node.text(
                    0) + obj_loaded.extension)
            self.obj_form = vehicle_W(obj_loaded, self.project_directory, self)
            self.obj_form.Form.show()

    def delete_object(self, node):
        self.delete_node(node)  # Delete the node
        self.project_framework.delete_file(self.project_directory, node.parent().text(0))  # Delete the file
