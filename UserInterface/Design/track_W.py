import sys
import os
from PyQt5 import QtWidgets
from PyQt5.QtGui import QStandardItem, QStandardItemModel
from PyQt5.QtWidgets import QMessageBox, QListView
from Design.track import Track
from UserInterface.QT_UI.track_ui import Ui_track
import numpy as np

from matplotlib.backends.qt_compat import QtCore, QtWidgets, is_pyqt5
if is_pyqt5():
    from matplotlib.backends.backend_qt5agg import (
        FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
else:
    from matplotlib.backends.backend_qt4agg import (
        FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
from matplotlib.figure import Figure

class track_W:
    def __init__(self, track: Track, project_directory, tree_widget):
        self.Form = QtWidgets.QWidget()
        self.ui = Ui_track()
        self.ui.setupUi(self.Form)
        self.track = track
        self.project_directory = project_directory
        self.tree_widget = tree_widget


        # Add the matplotlib to the form
        self.static_canvas = FigureCanvas(Figure(figsize=(5, 3)))
        self.ui.horizontalLayout_2.addWidget(self.static_canvas)



        if self.track is None:
            self.ui.lineEdit_Name.setText("")

            # Flag to know that we are creating a new object. Initialize an empty object
            self.is_new_obj = True
            self.track = Track()

        else:
            self.ui.lineEdit_Name.setText(str(self.track.name))

            self.is_new_obj = False

        self.ui.pushButton_OK.clicked.connect(self.on_pushButton_OK_clicked)
        self.ui.pushButton.clicked.connect(self.on_pushButton_clicked)



    def on_pushButton_clicked(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        desktop_path = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop')
        file_path_directory = QtWidgets.QFileDialog.getOpenFileName(None, "Import Track",
                                                                    desktop_path,
                                                                    "DAT (*.dat)",
                                                                    options=options)

        if file_path_directory == "": return #If user didn't select anything
        self.track.import_track(file_path_directory[0])
        #plot
        self._static_ax = self.static_canvas.figure.subplots()
        self._static_ax.plot(self.track.circuit[:, 0], self.track.circuit[:, 1])
        self._static_ax.legend([self.track.name])
        self._static_ax.set_xlabel("Longitudinal Coordinate [m]")
        self._static_ax.figure.canvas.draw()

        # List
        #TODO: Necessary to add the header text.
        model = QStandardItemModel()
        model.setColumnCount(2)
        for i in range(len(self.track.circuit[:,0])):
            long_coord = QStandardItem(str(self.track.circuit[i, 0]))
            lat_coord = QStandardItem(str(self.track.circuit[i, 1]))
            model.appendRow([long_coord,lat_coord])
        self.ui.tableView.setModel(model)


    def on_pushButton_OK_clicked(self):

        # Pass the values from the form to the object
        self.track.name = str(self.ui.lineEdit_Name.text())

        if self.is_new_obj:

            # If there is no duplicated name then save
            if not self.tree_widget.is_name_already_exists(TreeViewNames.circuit.value,
                                                           self.track.name):
                self.track.save_object(self.project_directory + "/" + TreeViewNames.circuit.value)
                self.tree_widget.add_obj_to_tree(self.track, TreeViewNames.circuit.value)
                self.Form.close()
            # If there is a duplicated name then display a box to ask what the user wants to do>
            # Override, cancel or discard.
            else:
                ret = self.tree_widget.msg_box_name_already_exists()
                if ret == QMessageBox.Save:
                    self.track.save_object(self.project_directory + "/" + TreeViewNames.circuit.value)
                    self.Form.close()
                elif ret == QMessageBox.Discard:
                    self.Form.close()
                elif ret == QMessageBox.Cancel:
                    pass
        else:  # If it's not a new object then simply save it
            self.track.save_object(self.project_directory + "/" + TreeViewNames.circuit.value)
            self.Form.close()