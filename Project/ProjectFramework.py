import json
import logging
import os
import re
import socket
from datetime import datetime
from enum import Enum


class DesignDirectory(Enum):
    aerodynamics = "/Aerodynamics"
    tyres = "/Tyres"
    chassis = "/Chassis"
    Vehicle = "/Vehicle"
    Results = "/Results"
    Powertrain = "/Powertrain"
    circuit = "/Circuit"


class ProjectFramework:
    def __init__(self, name: str, extension: str, version: str):
        self.name = name
        self.version = version
        self.date = ""
        self.user = ""
        self.extension = extension

    def create_project_folders(self, project_name, file_path):
        self.name = project_name
        self.version = "0.0.0"
        self.date = self.current_date_time()
        self.user = self.get_user()
        self.extension = ".wt"

        os.mkdir(file_path)
        os.mkdir(file_path + DesignDirectory.aerodynamics.value)
        os.mkdir(file_path + DesignDirectory.Powertrain.value)
        os.mkdir(file_path + DesignDirectory.tyres.value)
        os.mkdir(file_path + DesignDirectory.chassis.value)
        os.mkdir(file_path + DesignDirectory.Vehicle.value)
        os.mkdir(file_path + DesignDirectory.Results.value)
        os.mkdir(file_path + DesignDirectory.circuit.value)

        data = {
            "name": self.name,
            "version": self.version,
            "date": self.current_date_time(),
            "user": self.get_user(),
        }

        self.save_to_json(file_path, data)

    # Todo add the save and load conditions here
    def save_to_json(self, file_path, data):

        if not (self.is_name_empty() and self.is_name_already_existent() and self.is_name_special_character()):

            try:

                if file_path == "":
                    with open(self.name + self.extension, "w") as write_file:
                        json.dump(data, write_file)
                else:
                    with open(file_path + "\\" + self.name + self.extension, "w") as write_file:
                        json.dump(data, write_file)

            except Exception as ex:
                logging.exception('The following file was not successfully written: ' + self.name + self.extension)

    def load_from_json(self, file_path):
        data = {}
        try:

            with open(file_path, "r") as read_file:
                data = json.load(read_file)
                self.name = data.get("name")
                self.version = data.get("version")
                self.date = data.get("date")
                self.user = data.get("user")

        except Exception as ex:
            logging.exception('The following file was not successfully loaded: ' + self.name + self.extension)

        return data

    @staticmethod
    def current_date_time() -> str:
        return datetime.now().strftime("%d/%m/%Y %H:%M:%S")

    @staticmethod
    def get_user() -> str:
        return socket.gethostname()

    def is_name_empty(self) -> bool:
        if self.name == "":
            print("Error [0001] - The name is empty.")
            return True
        else:
            return False

    def is_name_already_existent(self) -> bool:
        if os.path.isfile("./" + self.name + self.extension):
            print("Error [0003] - File already exists.")
            return True
        else:
            return False

    def is_name_special_character(self) -> bool:
        if re.match("^[A-Za-z0-9]*$", self.name):
            print("Error [0002] - The name has a special symbol.")
            return True
        else:
            return False

    @staticmethod
    def delete_file(project_directory, node_name):

        obj_in_folder = os.listdir(project_directory + "\\" + node_name)
        for name in obj_in_folder:
            if name.split(".")[0] == node_name:
                os.remove(project_directory + "\\" + node_name + "\\" + name)
                break
