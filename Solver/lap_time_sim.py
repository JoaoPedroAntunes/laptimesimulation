import numpy as np
from Design import Results
from scipy import constants


def lap_time_sim(car, track):
    """
    This function computes the minimum lap time (in seconds) given car's
    parameters (Power P [W], mass m [kg], Cl_A [-], Cd_A [-]) and x y
    coordinates of the circuit.

    The code is quasi-steady state; the velocity is imposed as the
    maximum the car can reach in every point of the circuit.
    We perform a reverse lap to found the maximum velocities allowed for
    braking; then we calculate the actual velocities, which have to be
    lower than the the previously computed "reverse" velocities.

    This code is based on a single point mass approach and takes account
    of friction circle and aerodynamics. Friction coefficients mux and muy
    are assumed to be constant.

    :param: car:         Vehicle object that includes all parameters of the car
    :param: track:       Track object containing x y coordinates of the track
    :return: res:        Results structure

    """
    print()
    print("Running Simulation...")
    print("Loading Vehicle")

    # Car's local variables
    m = car.chassis.mass
    P = car.powertrain.power * 1000
    Cl_A = car.aerodynamics.get_CL_A()
    Cd_A = car.aerodynamics.get_Cd_A()
    mux = car.tyres.longitudinal_friction
    muy = car.tyres.lateral_friction

    print('Loading solver constants')
    rho = car.aerodynamics.air_density
    g = constants.g                     # gravitational acceleration, [m/s^2]
    v_max = 150.0                       # maximum velocity allowed, [m/s]

    # Calculate the curvature
    print("Calculating circuit curvature")
    track.compute_curvature()

    # Circuit's local variables
    circuit = track.circuit
    curvature = track.curvature

    print("Reverse Calculations")
    # __________________________________________________________________________________________________________________
    # __________________________________________________________________________________________________________________
    # REVERSE
    # __________________________________________________________________________________________________________________
    # __________________________________________________________________________________________________________________

    # I find the point with maximum curvature, which will be the starting
    # point for me. Then I re-order indexes to perform a reverse lap;
    # finally, I compute the maximum velocities for that point.
    i = int(np.argmax(abs(curvature)))
    index_initial_point = i
    indexes = np.concatenate((np.arange(i, -1, -1), (np.arange(len(curvature) - 1, i, -1))))

    # I compute the minimum curvature, based on the maximum velocity
    # previously defined. If a point has curvature lower than c_min, its
    # velocity will be set as v_max
    c_min = muy * g / (v_max ** 2) + 0.5 * (muy / m) * rho * Cl_A
    if abs(curvature[i]) > c_min:
        Vel_reverse = np.sqrt((muy * m * g) / (m * abs(curvature[i]) - 0.5 * rho * Cl_A * muy))
    else:
        Vel_reverse = v_max

    # I start the reverse cycle
    for i in range(len(indexes)):  # Calculation of the initial and final velocities of the part of the track considered
        v = Vel_reverse[len(Vel_reverse) - 1]
        if v > v_max:
            v = v_max
        if i == len(indexes) - 1:
            if abs(curvature[indexes[1]]) > c_min:
                v_minus_1 = np.sqrt((muy * m * g) / (m * abs(curvature[indexes[1]]) - 0.5 * rho * Cl_A * muy))
            else:
                v_minus_1 = v_max
        else:
            if abs(curvature[indexes[i + 1]]) > c_min:
                v_minus_1 = np.sqrt((muy * m * g) / (m * abs(curvature[indexes[i + 1]]) - 0.5 * rho * Cl_A * muy))
            else:
                v_minus_1 = v_max

        # Length of the part of track considered
        if i == len(indexes) - 1:
            length = np.sqrt((circuit[indexes[0]][0] - circuit[indexes[i]][0]) ** 2 +
                             (circuit[indexes[0]][1] - circuit[indexes[i]][1]) ** 2)
        else:
            length = np.sqrt((circuit[indexes[i+1]][0] - circuit[indexes[i]][0]) ** 2 +
                             (circuit[indexes[i+1]][1] - circuit[indexes[i]][1]) ** 2)

        # Drag and downforce computation at velocity v
        F_drag = 0.5 * Cd_A * rho * v ** 2
        F_lift = 0.5 * Cl_A * rho * v ** 2

        # Maximum Fy, equals to centrifugal force
        Fy = m * v ** 2 * curvature[indexes[i]]

        # Maximum Fx available for friction circle
        Fx_grip_squared = (mux * (m * g + F_lift)) ** 2 - (Fy * mux / muy) ** 2
        if Fx_grip_squared < 0:
            Fx_grip = 0
        else:
            Fx_grip = np.sqrt(Fx_grip_squared)

        # Braking force equals to the sum of Fx_grip and F_drag
        Fx = Fx_grip + F_drag
        a_braking = Fx / m

        # I calculate the final velocity for braking
        v_new = np.sqrt(v ** 2 + 2 * a_braking * length)        # v_fin = v_in + a_braking * t_fin
                                                                # 1/2 a_braking * t^2 + v_in * t + len = 0
                                                                # solve for t --> t_fin
        # Initial velocity update for braking
        if v_new > v_minus_1:
            v_new = v_minus_1

        Vel_reverse = np.append(Vel_reverse, v_new)

    Vel_reverse = np.flipud(Vel_reverse.reshape(-1, 1))         # row vector to column vector

    print("Forward Calculations")
    # __________________________________________________________________________________________________________________
    # __________________________________________________________________________________________________________________
    # FORWARD
    # __________________________________________________________________________________________________________________
    # __________________________________________________________________________________________________________________

    # Again, I find the point with maximum curvature, which will be the
    # starting point for me. Then I re-order indexes to perform a forward
    # lap; finally, I compute the maximum velocities for that point.
    i = int(np.argmax(abs(curvature)))
    indexes = np.concatenate((np.arange(i, len(curvature), 1), (np.arange(0, i, 1))))
    s = np.array([0])
    t = np.array([0])

    # If a point has curvature lower than c_min, its velocity will be set as v_max
    c_min = muy * g / (v_max ** 2) + 0.5 * (muy / m) * rho * Cl_A
    if abs(curvature[i]) > c_min:
        Vel = np.sqrt((muy * m * g) / (m * abs(curvature[i]) - 0.5 * rho * Cl_A * muy))
    else:
        Vel = v_max

    # Results vector initialization
    Fx_vec = np.array([0])
    Fy_vec = np.array([m * Vel ** 2 * curvature[i]])
    Drag_vec = np.array([0.5 * Cd_A * rho * Vel ** 2])
    Lift_vec = np.array([0.5 * Cl_A * rho * Vel ** 2])

    # I start the forward cycle
    for i in range(len(indexes)):
        # Calculation of the initial and final velocities of the part of
        # the track considered. If a point has curvature lower than c_min,
        # its velocity will be set as v_max
        v = Vel[len(Vel) - 1]
        if v > v_max:
            v = v_max
        if i == len(indexes) - 1:
            if abs(curvature[indexes[1]]) > c_min:
                v_plus_1 = np.sqrt((muy * m * g) / (m * abs(curvature[indexes[1]]) - 0.5 * rho * Cl_A * muy))
            else:
                v_plus_1 = v_max
        else:
            if abs(curvature[indexes[i + 1]]) > c_min:
                v_plus_1 = np.sqrt((muy * m * g) / (m * abs(curvature[indexes[i + 1]]) - 0.5 * rho * Cl_A * muy))
            else:
                v_plus_1 = v_max

        # Length of the part of track considered
        if i == len(indexes) - 1:
            length = np.sqrt((circuit[indexes[0]][0] - circuit[indexes[i]][0]) ** 2 +
                             (circuit[indexes[0]][1] - circuit[indexes[i]][1]) ** 2)
        else:
            length = np.sqrt((circuit[indexes[i+1]][0] - circuit[indexes[i]][0]) ** 2 +
                             (circuit[indexes[i+1]][1] - circuit[indexes[i]][1]) ** 2)

        # Drag and downforce computation at velocity v
        F_drag = 0.5 * Cd_A * rho * v ** 2
        F_lift = 0.5 * Cl_A * rho * v ** 2

        # Maximum Fy, equals to centrifugal force
        Fy = m * v ** 2 * curvature[indexes[i]]

        # Maximum Fx available for friction circle
        Fx_grip_squared = (mux * (m * g + F_lift)) ** 2 - (Fy * mux / muy) ** 2
        if Fx_grip_squared < 0:
            Fx_grip = 0
        else:
            Fx_grip = np.sqrt(Fx_grip_squared)

        # Fx computation: if F_engine - F_drag < Fx_grip ok, else I am limited by Fx_grip
        F_engine = P / v
        Fx = np.minimum(Fx_grip, (F_engine - F_drag))
        acceleration = Fx / m

        # Calculation of final velocity for maximum acceleration possible
        v_new = np.sqrt(v ** 2 + 2 * acceleration * length)

        # v_fin cannot be higher than v_max of the successive cornering
        if v_new > v_plus_1:
            v_new = v_plus_1

        # v_fin cannot be higher than Vel_reverse
        if i == len(indexes) - 1:
            if v_new > Vel_reverse[0]:
                v_new = Vel_reverse[0]
        else:
            if v_new > Vel_reverse[i+1]:
                v_new = Vel_reverse[i+1]

        # Results vector update
        Vel = np.append(Vel, v_new)
        s = np.append(s, length + s[len(s)-1])
        t_new = t[len(t)-1] + 2 * length / (v + v_new)
        t = np.append(t, t_new)
        Fx_vec = np.append(Fx_vec, Fx)
        Fy_vec = np.append(Fy_vec, m * v_new ** 2 * curvature[indexes[i]])
        Drag_vec = np.append(Drag_vec, 0.5 * Cd_A * rho * v_new ** 2)
        Lift_vec = np.append(Lift_vec, 0.5 * Cl_A * rho * v_new ** 2)

    Vel = Vel.reshape(-1, 1)
    s = s.reshape(-1, 1)
    t = t.reshape(-1, 1)
    lap_time = t[len(t)-1]
    Fx = Fx_vec.reshape(-1, 1)
    Fy = Fy_vec.reshape(-1, 1)
    Drag = Drag_vec.reshape(-1, 1)
    Lift = Lift_vec.reshape(-1, 1)

    # Re order velocity, time and distance vectors
    Vel_new = np.array([])
    s_new = np.array([])
    t_new = np.array([])
    Fx_new = np.array([])
    Fy_new = np.array([])
    Drag_new = np.array([])
    Lift_new = np.array([])
    shift1_s = s[len(s)-1-index_initial_point]
    shift1_t = t[len(t)-1-index_initial_point]
    for i in range(len(s)-1-index_initial_point, len(s)):
        s_new = np.append(s_new, s[i] - shift1_s)
        t_new = np.append(t_new, t[i] - shift1_t)
        Vel_new = np.append(Vel_new, Vel[i])
        Fx_new = np.append(Fx_new, Fx[i])
        Fy_new = np.append(Fy_new, Fy[i])
        Drag_new = np.append(Drag_new, Drag[i])
        Lift_new = np.append(Lift_new, Lift[i])

    shift2_s = s[len(s)-1] - s[len(s)-1-index_initial_point]
    shift2_t = t[len(t)-1] - t[len(t)-1-index_initial_point]
    for i in range(1, len(s)-index_initial_point):
        s_new = np.append(s_new, s[i] + shift2_s)
        t_new = np.append(t_new, t[i] + shift2_t)
        Vel_new = np.append(Vel_new, Vel[i])
        Fx_new = np.append(Fx_new, Fx[i])
        Fy_new = np.append(Fy_new, Fy[i])
        Drag_new = np.append(Drag_new, Drag[i])
        Lift_new = np.append(Lift_new, Lift[i])

    s = s_new.reshape(-1, 1)
    t = t_new.reshape(-1, 1)
    Vel = Vel_new.reshape(-1, 1)
    Fx = Fx_new.reshape(-1, 1)
    Fy = Fy_new.reshape(-1, 1)
    Drag = Drag_new.reshape(-1, 1)
    Lift = Lift_new.reshape(-1, 1)

    # Creating results' object
    res = Results()
    # res.import_result(Vel, s, t, lap_time, np.divide(Fx, m*g), np.divide(Fy, m*g), Drag, Lift)
    res.import_result(Vel, s, t, lap_time)

    return res

