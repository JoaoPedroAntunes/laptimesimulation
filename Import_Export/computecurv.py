import numpy as np


def circumference_center(A, B, C):
    """
    Center and radius of the circumscribed circle for the triangle ABC. Works in 2D and 3D.
    The formula used is G = ( ||C-A||^2 [(B-A)x(C-A)x(B-A)] - ||B-A||^2 [(B-A)x(C-A)x(C-A)] ) / ( 2||(B-A)x(C-A)||^2 )
    where G is the vector from A to the circumference_center.

    :param: A:      3D coordinate vectors for the triangle corners (point 1)
    :param: B:      3D coordinate vectors for the triangle corners (point 2)
    :param: C:      3D coordinate vectors for the triangle corners (point 3)
    :return: R:     Radius of curvature
    :return: k:     Vector of length 1/R in the direction from A towards M (Curvature vector)
    :return: M:     3D coordinate vector for the center
    """

    D = np.cross(B-A, C-A)
    b = np.linalg.norm(A-C)
    c = np.linalg.norm(A-B)
    E = np.cross(D, B-A)
    F = np.cross(D, C-A)
    G = (b**2 *E - c**2 *F) / np.linalg.norm(D)**2 /2
    M = A+G
    R = np.linalg.norm(G)                     # Radius of curvature
    if R == 0:
        k = G
    else:
        k = np.transpose(G)/R**2              # Curvature vector

    return R, k, M


def compute_curvature(X):
    """
    Radius of curvature and curvature vector for 2D or 3D curve, modified for closed curves.
    The curve is modified adding a first entry equal to the last one and a last entry equal to the first one.
    Then it calculates the curvature vector with the circumference_center function.
    Finally, it uses the cross product between curvature vector and vector from point X[i-1] to X[i] for
    computing the sign of the curvature.

    :param: X:      2 or 3 column array of x, y (and possibly z) coordinates
    :return: R:     Radius of curvature
    :return: k:     Curvature vector
    :return: L:     Cumulative arc of length
    :return: curv:  Curvature modulus (with sign)
    """

    N = len(X)
    dims = len(X[1][:])
    if dims == 2:
        X = np.concatenate((X, np.zeros((N, 1))), axis=1)     # Do all calculations in 3D

    X = np.insert(X, 0, X[len(X)-1][:], axis=0)               # first entry equal to the last one
    X = np.append(X, X[1][:].reshape(1, -1), axis=0)          # last entry equal to the first one
    L = np.zeros((N+2, 1))
    R = np.zeros((N+2, 1))
    k = np.zeros((N+2, 3))
    curv = np.zeros((N+2, 1))

    for i in range(1, N+1):
        R[i], k[i][:], _ = circumference_center(np.transpose(X[i][:]), np.transpose(X[i-1][:]), np.transpose(X[i+1][:]))
        L[i] = L[i-1] + np.linalg.norm(X[i][:] - X[i-1][:])
        vect = np.cross(k[i][:], X[i][:] - X[i-1][:])
        if vect[2] > 0:
            curv[i] = np.linalg.norm(k[i][:])
        else:
            curv[i] = -np.linalg.norm(k[i][:])

    L = L[1:curv.size - 1]                  # I delete first and last entry of the vectors, which are null entries
    R = R[1:curv.size - 1]
    k = k[1:curv.size - 1][:]
    curv = curv[1:curv.size - 1]

    if dims == 2:
        k = np.delete(k, 2, 1)

    return curv, k, L, R
